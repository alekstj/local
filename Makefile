DBDIR = /var/db/repos/local
SYNC = rsync
USER = portage

all:
	@echo "push or pull"

pull:
	$(SYNC) -Parvu $(DBDIR)/* .

push:
	$(SYNC) --exclude='.git/' -Parvu . $(DBDIR)/
	chown -R $(USER):$(USER) $(DBDIR)
